

import org.junit.Test;

import junit.framework.Assert;

public class OperacoesTest {
	
	@Test
	@SuppressWarnings("deprecation")
	public void somaTest() {
		Operacoes op = new Operacoes(3, 3);
		int result = op.soma();
		Assert.assertEquals(6, result);
	}
	
	@Test
	@SuppressWarnings("deprecation")
	public void subtracaoTest() {
		Operacoes op = new Operacoes(5, 3);
		int result = op.subtracao();
		Assert.assertEquals(2, result);
	}

}
